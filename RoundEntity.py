class RoundEntity():
    # Example of graph
    # GRAPH = { "Nodo_A" : {"ip":"127.0.0.2:5000", "latency": 0, "mu": 1.0, "speed": 1.0},
    #      "Nodo_B" : {"ip":"127.0.0.3:5000", "latency": 0, "mu": 1.0, "speed": 1.0}, 
    #      "Nodo_C" : {"ip":"127.0.0.4:5000", "latency": 0, "mu": 1.0, "speed": 1.0}
    #    }

    def __init__(self,graph, round_number,time):
        self.round_number = round_number
        self.mixed_queue_ = 0
        self.timestamp = time
        self.graph = graph

    def set_mixed_queue(self,mixed_queue_):
        self.mixed_queue_ = mixed_queue_

    def set_lat(self, node, lat):
        self.graph[node]["latency"] = lat

    def set_mu(self, node, mu):
        self.graph[node]["mu"] = mu   

    def get_lat(self, node):
        return self.graph[node]["latency"]

    def get_mu(self, node):
        return self.graph[node]["mu"]
    
    def show_details(self):
        ret = "Round Number: "+str(self.round_number)+"\n"
        ret += "Timestamp: "+str(self.timestamp)+"\n"
        ret += "Graph: "+str(self.graph)+"\n"
        return ret